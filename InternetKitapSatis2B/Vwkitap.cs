﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InternetKitapSatis2B
{
    public class Vwkitap
    {

        public int ID { get; set; }
        public string Adi { get; set; }
        public String BasimYili { get; set; }
        public int BasimYeriID { get; set; }
        public string BasimYeri { get; set; }
        public int YayinEviID { get; set; }
        public string YayinEvi { get; set; }
        public DateTime KayitTarihi { get; set; }
        public int KaydedenID { get; set; }
        public string KaydedenAdi { get; set; }
        public string KaydedenSoyadi { get; set; }
        public DateTime DegisiklikTarihi { get; set; }
        public int DegistirenID { get; set; }
        public string DegistirenAdi { get; set; }
        public string DegistirenSoyadi { get; set; }

        public string KaydedenSoyAdi { get; set; }

        public string DegistirenSoyAdi { get; set; }
    }
}