﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InternetKitapSatis2B
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Application["Sayac"] == null)
            {
                Application["Sayac"] = 1;
                Label5.Text = Application["Sayac"].ToString();
            }
            else
            {
                int sayi = (int)Application["Sayac"];
                sayi++;
                Application["Sayac"] = sayi;
                Label5.Text = Application["Sayac"].ToString();

            }
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {

            Kullanici x = new Kullanici();

            using (SqlConnection conNEsne = new SqlConnection(ConfigurationManager.ConnectionStrings["constring"].ToString()))
            {
                List<Kullanici> list = new List<Kullanici>();

                conNEsne.Open();
                SqlCommand komut = new SqlCommand("select * from Kullanici where Email= '" + TextBox1.Text + "'and Sifre='" + TextBox2.Text + "'", conNEsne);
                SqlDataReader satirlar = komut.ExecuteReader();
                while (satirlar.Read())
                {
                    Kullanici k = new Kullanici();

                    k.ID = (int)satirlar["ID"];
                    k.Ad = (String)satirlar["Ad"];
                    k.Soyad = (String)satirlar["Soyad"];
                    k.Email = (String)satirlar["Email"];
                    k.Sifre = (String)satirlar["Sifre"];
                    k.KayitEdenID = (int)satirlar["KayitEdenID"];
                    k.KayitTarihi = (DateTime)satirlar["KayitTarihi"];
                    // k.DegistirenID = (int)satirlar["DegistirenID"];
                    //k.DegisiklikTarihi = (DateTime)satirlar["DegisiklikTarihi"];
                    k.RolID = (int)satirlar["RolID"];

                    list.Add(k);
                }
                if (list.Count == 0)
                {

                    Label3.Text = "Giriş başarisiz";
                    Label3.Visible = true;


                }
                else
                {
                    Session["GirenUye"] = list[0];
                     Response.Redirect("üye.aspx");
                   Label3.Text = "Giriş basarili";
                   Label3.Visible = true;

                }

            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("WebForm5.aspx");
        }
    }
}
