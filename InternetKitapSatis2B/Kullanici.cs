﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InternetKitapSatis2B
{
    public class Kullanici
    {
        public int ID;
        public string Ad;
        public string Soyad;
        public string Email;
        public string Sifre;
        public int KayitEdenID;
        public DateTime KayitTarihi;
        public int DeğiştiştirenID;
        public DateTime DeğişiklikTarihi;
        public int RolID;
    }
}