﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm5.aspx.cs" Inherits="InternetKitapSatis2B.WebForm5" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1
        {
            width: 100%;
            height: 721px;
            background-image: url('images/images%20(1).jpg');
            margin-right: 341px;
        }
        .auto-style2
        {
            height: 167px;
            width: 1029px;
        }
        .auto-style3
        {
            width: 1056px;
            height: 166px;
        }
        .auto-style4
        {
            height: 53px;
            width: 1029px;
        }
        .auto-style5
        {
            width: 101%;
            height: 54px;
        }
        .auto-style6
        {
            width: 169px;
        }
        .auto-style7
        {
            width: 176px;
        }
        .auto-style8
        {
            width: 150px;
        }
        .auto-style9
        {
            width: 100%;
            height: 1072px;
            margin-right: 5px;
        }
        .auto-style14
        {
            width: 278px;
            height: 55px;
        }
        .auto-style15
        {
            height: 55px;
            width: 229px;
        }
        .auto-style16
        {
            width: 278px;
            height: 177px;
        }
        .auto-style17
        {
            height: 177px;
            width: 229px;
        }
        .auto-style18
        {
            width: 278px;
            height: 52px;
        }
        .auto-style19
        {
            height: 52px;
            width: 229px;
        }
        .auto-style20
        {
            width: 278px;
            height: 182px;
        }
        .auto-style21
        {
            height: 182px;
            width: 229px;
        }
        .auto-style23
        {
            width: 278px;
            height: 46px;
        }
        .auto-style24
        {
            height: 46px;
            width: 229px;
        }
        .auto-style25
        {
            width: 278px;
            height: 44px;
        }
        .auto-style26
        {
            height: 44px;
            width: 229px;
        }
        .auto-style27
        {
            width: 1029px;
        }
        .auto-style29
        {
            height: 55px;
            width: 34px;
        }
        .auto-style30
        {
            height: 177px;
            width: 34px;
        }
        .auto-style31
        {
            height: 52px;
            width: 34px;
        }
        .auto-style32
        {
            height: 182px;
            width: 34px;
        }
        .auto-style33
        {
            height: 46px;
            width: 34px;
        }
        .auto-style34
        {
            height: 44px;
            width: 34px;
        }
        .auto-style35
        {
            width: 290px;
            height: 379px;
        }
        .auto-style36
        {
            width: 278px;
            height: 23px;
        }
        .auto-style37
        {
            height: 23px;
            width: 229px;
        }
        .auto-style38
        {
            height: 23px;
            width: 34px;
        }
        .auto-style39
        {
            width: 278px;
            height: 377px;
            margin-left: 0px;
        }
        .auto-style40
        {
            width: 276px;
            height: 376px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">
                    <img alt="" class="auto-style3" src="images/BookBanner.jpg" /></td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <table class="auto-style5">
                        <tr>
                            <td class="auto-style6">
                                <asp:Button ID="Button1" runat="server" Height="55px" Text="Anasayfa" Width="205px" />
                            </td>
                            <td class="auto-style7">
                                <asp:Button ID="Button2" runat="server" Height="55px" Text="Çok Satanlar" Width="205px" />
                            </td>
                            <td class="auto-style7">
                                <asp:Button ID="Button3" runat="server" Height="55px" Text="En Yeniler" Width="205px" />
                            </td>
                            <td class="auto-style8">
                                <asp:Button ID="Button4" runat="server" Height="55px" Text="Yazarlar" Width="205px" />
                            </td>
                            <td>
                                <asp:Button ID="Button5" runat="server" Height="55px" Text="Yayın Evleri" Width="216px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="auto-style27">
                    <table class="auto-style9">
                        <tr>
                            <td class="auto-style36">
                                <img alt="" class="auto-style35" src="images/elifsafak.jpg" /></td>
                            <td class="auto-style37">
                                <img alt="" class="auto-style39" src="images/ermektubu.jpg" /></td>
                            <td class="auto-style38">
                                <img alt="" class="auto-style40" src="images/kurtseyit.jpg" /></td>
                        </tr>
                        <tr>
                            <td class="auto-style14"></td>
                            <td class="auto-style15"></td>
                            <td class="auto-style29"></td>
                        </tr>
                        <tr>
                            <td class="auto-style14"></td>
                            <td class="auto-style15"></td>
                            <td class="auto-style29"></td>
                        </tr>
                        <tr>
                            <td class="auto-style16"></td>
                            <td class="auto-style17"></td>
                            <td class="auto-style30"></td>
                        </tr>
                        <tr>
                            <td class="auto-style18"></td>
                            <td class="auto-style19"></td>
                            <td class="auto-style31"></td>
                        </tr>
                        <tr>
                            <td class="auto-style18"></td>
                            <td class="auto-style19"></td>
                            <td class="auto-style31"></td>
                        </tr>
                        <tr>
                            <td class="auto-style20"></td>
                            <td class="auto-style21"></td>
                            <td class="auto-style32"></td>
                        </tr>
                        <tr>
                            <td class="auto-style23"></td>
                            <td class="auto-style24"></td>
                            <td class="auto-style33"></td>
                        </tr>
                        <tr>
                            <td class="auto-style25"></td>
                            <td class="auto-style26"></td>
                            <td class="auto-style34"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
