﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm3.aspx.cs" Inherits="InternetKitapSatis2B.WebForm3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #form1
        {
            height: 154px;
            width: 891px;
        }
        .auto-style1
        {
            width: 100%;
            height: 140px;
            background-image: url('images/images%20(1).jpg');
        }
        .auto-style11
        {
            width: 210px;
        }
        .auto-style2
        {
            width: 220px;
            height: 151px;
            margin-bottom: 5px;
        }
        .auto-style10
        {
            width: 128px;
        }
        .auto-style4
        {
            width: 188px;
            height: 161px;
            margin-right: 3px;
        }
        .auto-style6
        {
            width: 182px;
        }
        .auto-style9
        {
            width: 187px;
            height: 159px;
            margin-bottom: 2px;
        }
        .auto-style7
        {
            width: 193px;
            height: 163px;
        }
        .auto-style8
        {
            width: 191px;
            height: 156px;
        }
        .auto-style3
        {
            width: 220px;
            height: 180px;
        }
        .auto-style12
        {
            width: 191px;
            height: 181px;
        }
        .auto-style13
        {
            width: 185px;
            height: 184px;
        }
        .auto-style14
        {
            width: 197px;
            height: 184px;
        }
        .auto-style15
        {
            width: 195px;
            height: 185px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td>
<table width="1021" height="1036" border="0" align="center">
  <tr>
    <td height="146" colspan="6">
        <img class="auto-style1" src="images/BookBanner.jpg" /></td>
  </tr>
  <tr>
    <td height="66" colspan="6"><table width="1019" height="67" border="0">
      <tr>
        <td width="199" height="61">
            <asp:Button ID="Button1" runat="server" Height="55px" Text="Anasayfa" Width="196px" />
          </td>
        <td width="199">
            <asp:Button ID="Button2" runat="server" Height="55px" Text="Çok Satanlar" Width="196px" OnClick="Button2_Click" />
          </td>
        <td width="199">
            <asp:Button ID="Button3" runat="server" Height="55px" Text="En Yeniler " Width="196px" />
          </td>
        <td width="199">
            <asp:Button ID="Button4" runat="server" Height="55px" Text="Yazarlar" Width="196px" />
          </td>
        <td width="199">
            <asp:Button ID="Button5" runat="server" Height="55px" Text="Yayın Evleri" Width="196px" />
          </td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td height="75" colspan="3"  >&nbsp; &nbsp;<asp:Label ID="Label1" runat="server" Text="E-mail :" BackColor="#CCCCCC"></asp:Label>
&nbsp;<asp:TextBox ID="TextBox1" runat="server" align="center" Width="233px"></asp:TextBox>
      </td>
    <td height="75" colspan="2" >
        <asp:Label ID="Label2" runat="server" Text="Şifre :" BackColor="#CCCCCC"></asp:Label>
&nbsp;<asp:TextBox ID="TextBox2" runat="server" Width="147px" TextMode="Password"></asp:TextBox>
      </td>
    <td height="75" >
        <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/WebForm4.aspx">kayıt</asp:LinkButton>
&nbsp;&nbsp;
        <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click">giriş yap</asp:LinkButton>
        <asp:Label ID="Label3" runat="server" Text="Label" Visible="False"></asp:Label>
      </td>
  </tr>
  
  <tr>
    <td height="41" align="center" class="auto-style11"><span class="style3" style="color: #ccc" >En &Ccedil;ok Satanlar </span></td>
    <td width="6" rowspan="9" style="color: #ccc" >&nbsp;</td>
    <td height="41" colspan="4" align="center"><span class="style1" style="color: #CCCCCC">HAFTANIN K&#304;TAPLARI </span></td>
  </tr>
  <tr>
    <td height="165" class="auto-style11">
        <asp:Image ID="Image1" runat="server" ImageAlign="Right" />
        <img class="auto-style2" src="images/kitap2.jpg" /></td>
    <td height="165" class="auto-style10">
        <img class="auto-style4" src="images/446524.jpg" /></td>
    <td height="165"class="auto-style6">
        <img class="auto-style9" src="images/599406.jpg" /></td>
    <td height="165">
        <img class="auto-style7" src="images/611966.jpg" /></td>
    <td width="191" height="165">
        <img class="auto-style8" src="images/599491.jpg" /></td>
  </tr>
  <tr>
    <td height="80" rowspan="3" align="center" class="auto-style11" style="color: #ccc"><p class="style8">Varl&#305;k ve Hi&ccedil;lik<br />
      &amp;<br />
    Jean Paul Sartre</p>
    <p class="style8"><span class="style10">30,00 TL(kdv dahil)</span></p></td>
    <td height="61" align="center" class="auto-style10" style="color: #ccc"><p class="style8">Tolkien'in A&#287;ac&#305;&nbsp;&nbsp;&nbsp;&nbsp; </p>
    <p class="style9">Derleme</p></td>
    <td height="61" align="center" class="auto-style6"><table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tbody>
        <tr>
          <td valign="top"><span class="style8" style="color: #ccc">Neoliberalizm, &#304;slamc&#305; Sermayenin Y&uuml;kseli&#351;i ve Akp</span></td>
        </tr>
      </tbody>
    </table>
      <br />
      <p style="color: #ccc"><span class="style9">Ne&#351;ecan Balkan </span></p>
      <p><br />
        </p></td><td height="61" align="center" style="color: #ccc"><p class="style8">Gelibolu 1915</p>
          <p class="style9">Peter Hart </p></td>
    <td height="61" align="center" style="color: #ccc"><p class="style8">Masumiyetin Sonu</p>
    <p class="style9">Susie Orbach</p></td>
  </tr>
  <tr>
    <td align="center" class="auto-style10" style="color: #ccc"><span class="style10">8,50 TL </span></td>
    <td height="23" align="center" class="auto-style6" style="color: #ccc"><span class="style10">17,25 TL </span></td>
    <td height="23" align="center" style="color: #ccc"><span class="style10">21,75 TL </span></td>
    <td height="23" align="center"><span class="style10" style="color: #ccc">13,50 TL </span></td>
  </tr>
  <tr>
    <td align="center" class="auto-style10" style="color: #ccc"><span class="style10">(kdv dahil) </span></td>
    <td height="23" align="center" class="auto-style6" style="color: #ccc"><span class="style10">(kdv dahil)</span></td>
    <td height="23" align="center" style="color: #ccc"><span class="style10">(kdv dahil)</span></td>
    <td height="23" align="center" style="color: #ccc"><span class="style10">(kdv dahil)</span></td>
  </tr>
  
  <tr>
    <td height="184" class="auto-style11">
        <img class="auto-style3" src="images/kitap.jpg" /></td>
    <td height="184" class="auto-style10">
        <img class="auto-style12" src="images/611976.jpg" /></td>
    <td height="184" class="auto-style6">
        <img class="auto-style13" src="images/609979.jpg" /></td>
    <td height="184">
        <img class="auto-style14" src="images/612017.jpg" /></td>
    <td height="184">
        <img class="auto-style15" src="images/611965.jpg" /></td>
  </tr>
  <tr>
    <td height="71" rowspan="3" align="center" class="auto-style11" style="color: #ccc"><span class="style8">Slovaj Zizek<br />
Sinema seti<br />
&amp;&nbsp; </span><br />
<span class="style10">24,00 TL(kdv dahil)</span></td>
    <td height="57" align="center" class="auto-style10" style="color: #ccc"><p class="style8">Kam&ccedil;atka</p>
    <p class="style9">Marcelo F&#305;gueras</p></td>
    <td height="57" align="center" class="auto-style6" style="color: #ccc"><p class="style8">Kocan Kadar Konu&#351;</p>
    <p class="style9">&#350;ebnem Burcuo&#287;lu</p></td>
    <td height="57" align="center" style="color: #ccc"><p class="style8">Alt&#305;n &Uuml;lke : &Ccedil;ocukluk</p>
    <p class="style9">Kolektif</p></td>
    <td height="57" align="center" style="color: #ccc"><p class="style8">Bir Beyaz T&uuml;rk'&uuml;n Haf&#305;za Defteri </p>    </td>
  </tr>
  <tr>
    <td height="35" align="center" class="auto-style10" style="color: #ccc"><span class="style10">20,25 TL </span></td>
    <td height="-1" align="center" class="auto-style6" style="color: #ccc"><span class="style10">14,25 TL</span></td>
    <td height="-1" align="center" style="color: #ccc"><span class="style10">12,35 TL</span></td>
    <td height="-1" align="center" style="color: #ccc"><span class="style10">18,75 TL</span></td>
  </tr>
  <tr>
    <td height="21" align="center" class="auto-style10" style="color: #ccc"><span class="style10">(kdv dahil)</span></td>
    <td height="21" align="center" class="auto-style6" style="color: #ccc"><span class="style10">(kdv dahil)</span></td>
    <td height="21" align="center" style="color: #ccc"><span class="style10">(kdv dahil)</span></td>
    <td height="21" align="center" style="color: #ccc"><span class="style10">(kdv dahil)</span></td>
  </tr>
  <tr>
    <td colspan="3" align="center" style="color: #ccc">
        <asp:Label ID="Label4" runat="server" Text="Ziyaretçi Sayısı:"></asp:Label>
        <asp:Label ID="Label5" runat="server"></asp:Label>
      </td>
    <td colspan="3" align="center" style="color: #ccc"><span class="style8">&copy;&nbsp; 2014 Kitap Sat&#305;&#351; webmaster: &Ouml;zcan Tuncer &amp; Mehmet &#350;irin &amp; Mert Sa&#287;ra</span></td>
  </tr>
</table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
