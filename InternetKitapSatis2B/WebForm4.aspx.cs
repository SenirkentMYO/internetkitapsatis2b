﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InternetKitapSatis2B
{
    public partial class WebForm4 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Application["Sayac"] == null)
            {
                Application["Sayac"] = 1;
                Label1.Text = Application["Sayac"].ToString();
            }
            else
            {
                int sayi = (int)Application["Sayac"];
                sayi++;
                Application["Sayac"] = sayi;
                Label1.Text = Application["Sayac"].ToString();
            }
        }
    }
}