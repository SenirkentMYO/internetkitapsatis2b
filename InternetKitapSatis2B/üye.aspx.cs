﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InternetKitapSatis2B
{
    public partial class üye : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<Vwkitap> list = new List<Vwkitap>();

            using (SqlConnection conNEsne = new SqlConnection(ConfigurationManager.ConnectionStrings["constring"].ToString()))
            {
                conNEsne.Open();
            SqlCommand veri = new SqlCommand("Select * from VwKitap", conNEsne);
            SqlDataReader satir = veri.ExecuteReader();
            while (satir.Read())
            {
                Vwkitap a = new Vwkitap();
                a.ID = (int)satir["ID"];
                a.Adi = (String)satir["Adi"];
                a.BasimYili = (String)satir["BasimYili"];
                a.BasimYeri = (String)satir["BasimYeri"]; ;
                a.YayinEviID = (int)satir["YayinEviID"];
                a.YayinEvi = (String)satir["YayinEvi"];
                a.KayitTarihi = (DateTime)satir["KayitTarihi"];
                a.KaydedenID = (int)satir["KaydedenID"];
                a.KaydedenAdi = (String)satir["KaydedenAdi"];
                a.KaydedenSoyAdi = (string)satir["KaydedenSoyadi"];
                a.DegisiklikTarihi = (DateTime)satir["DegisiklikTarihi"];
                a.DegistirenID = (int)satir["DegistirenID"];
                a.DegistirenAdi = (String)satir["DegistirenAdi"];
                a.DegistirenSoyAdi = (string)satir["DegistirenSoyadi"];

                list.Add(a);

            }
          
            DataList1.DataSource = list;
            DataList1.DataBind();

            if (Session["GirenUye"] == null)
            {
                Response.Redirect("default.aspx");
            }
            else
            {
                Kullanici k = ((Kullanici)Session["GirenUye"]);
                Label1.Text = "Hoşgeldiniz," + k.Ad + " " + k.Soyad;

            }
            if (Application["Sayac"] == null)
            {
                Application["Sayac"] = 1;
                //Label2.Text = Application["Sayac"].ToString();
            }
            else
            {
                int sayi = (int)Application["Sayac"];
                sayi++;
                Application["Sayac"] = sayi;
            }
        }
    }
}
}